import { navigate } from "gatsby-link"
import React from "react"
import { Flex, Text, Button, Box } from "rebass"
import useMedusa from "../../../hooks/use-medusa"
import {
  Table,
  TableHead,
  TableHeaderCell,
  TableHeaderRow,
  TableBody,
  TableRow,
  TableDataCell,
  DefaultCellContent,
} from "../../../components/table"
import Spinner from "../../../components/spinner"

const UsersIndex = () => {
  const { users } = useMedusa("users")

  return (
    <Flex flexDirection="column" pb={5} pt={5}>
      <Flex>
        <Text mb={3} fontSize={20} fontWeight="bold">
          Users
        </Text>
        <Box ml="auto" />
        <Button
          variant="primary"
          onClick={() => navigate("/a/settings/users/new")}
        >
          + Add new user
        </Button>
      </Flex>
      {!users ? (
        <Spinner />
      ) : (
        <Table>
          <TableHead>
            <TableHeaderRow>
              <TableHeaderCell>Name</TableHeaderCell>
              <TableHeaderCell>Role</TableHeaderCell>
            </TableHeaderRow>
          </TableHead>
          <TableBody>
            {users.map(({ id, first_name, last_name, role }) => {
              const userFullName = first_name
                ? `${first_name} ${last_name}`
                : "-"
              return (
                <TableRow key={id}>
                  <TableDataCell>
                    <DefaultCellContent>{userFullName}</DefaultCellContent>
                  </TableDataCell>
                  <TableDataCell>
                    <DefaultCellContent>{role}</DefaultCellContent>
                  </TableDataCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      )}
    </Flex>
  )
}

export default UsersIndex
