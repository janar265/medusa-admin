import React from "react"
import { useForm } from "react-hook-form"
import { Flex, Text, Box } from "rebass"
import Input from "../../../components/input"
import Button from "../../../components/button"
import Medusa from "../../../services/api"
import { navigate } from "gatsby-link"

const NewUser = () => {
  const { register, handleSubmit } = useForm()

  const onSave = data => {
    data.role = "admin"
    Medusa.users.create(data).then(({ data }) => {
      navigate(`/a/settings/users`)
    })
  }

  return (
    <Flex
      as="form"
      onSubmit={handleSubmit(onSave)}
      flexDirection="column"
      pt={5}
      alignItems="center"
      justifyContent="center"
      width="100%"
    >
      <Flex width={3 / 5} justifyContent="flex-start">
        <Text mb={4} fontWeight="bold" fontSize={20}>
          Create new user
        </Text>
      </Flex>
      <Flex mb={5} width={3 / 5} flexDirection="column">
        <Input
          required={true}
          mb={3}
          type="email"
          name="email"
          label="Email"
          ref={register}
          width="75%"
        />
        <Input
          mb={3}
          name="first_name"
          label="Firstname"
          ref={register}
          width="75%"
        />
        <Input
          mb={3}
          name="last_name"
          label="Lastname"
          ref={register}
          width="75%"
        />
        <Input
          mb={3}
          type="password"
          required={true}
          name="password"
          label="Password"
          ref={register}
          width="75%"
        />
        <Flex mt={4} width="75%">
          <Box ml="auto" />
          <Button variant={"cta"} type="submit">
            Create
          </Button>
        </Flex>
      </Flex>
    </Flex>
  )
}

export default NewUser
